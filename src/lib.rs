extern crate serde;
extern crate serde_json;

#[macro_use]
extern crate serde_derive;

extern crate ring;
use ring::signature::Ed25519KeyPair;
use ring::{signature,digest};

extern crate untrusted;
use untrusted::Input;

use std::collections::HashMap;
use std::time::{UNIX_EPOCH,SystemTime};

pub mod error;
use error::*;

#[cfg(test)]
mod test;

pub type Channel = u32;

//Should this be a vec or a slice?
pub type Name = Vec<u8>; 

//Posts with the same PostID are rejected
pub type PostId = Vec<u8>;

//Public key
pub type Key = Vec<u8>;

//Signature
pub type Signature = Vec<u8>;


pub type TrustLevel = f64;

#[derive(Deserialize,Serialize,Clone,PartialEq,Debug)]
pub enum TrustOpt {
    NoPropogate, //Only trust for yourself
    Leaf, //User can't trust others
    //RestrictTopic(String) //Limit trust to a specific subforum/topic
}

#[derive(Clone,Serialize,Deserialize,PartialEq,Debug)]
pub enum Action {
    Vote(PostId, Channel),
    Name(Key, Channel, Name),
    Trust(Key, ActionType, TrustLevel, Vec<TrustOpt>),
    Post{content: PostContent, parent: Option<PostId>},
    Edit(PostId, PostContent), //diffs are handled client side
}

#[derive(Copy,Clone,Serialize,Deserialize,PartialEq,Debug)]
pub enum ActionType {
    Vote,
    Name,
    Trust,
    Post,
    Edit
}

#[derive(Clone,Serialize,Deserialize,PartialEq,Debug)]
pub struct PostContent {
    title: String,
    content: String,
    tags: Vec<String>
}

#[derive(Clone,Serialize,Deserialize,PartialEq,Debug)]
pub struct Message {
    public_key: Vec<u8>,
    signature: Vec<u8>,
    action: Action,
    timestamp: String, //String with time in milliseconds
}

impl Message {
    pub fn new<'b>(keypair: &'b Ed25519KeyPair, action: Action) -> Result<Message> {
		let time = SystemTime::now().duration_since(UNIX_EPOCH)?;
        Ok(Message {
            //The keypair can probably just be a reference
			public_key: keypair.public_key_bytes().to_vec(),
			timestamp: format!("{}{}",time.as_secs(),time.subsec_nanos()),
            signature: keypair.sign(
                format!("{}{}{}",serde_json::to_string(&action)?,time.as_secs(),time.subsec_nanos()).as_bytes()
                ).as_ref().to_vec(),
			action,
               
        })
    }
    pub fn verify(&self) -> Result<()> {
        let ser = serde_json::to_string(&self.action)?;
        let msg = format!("{}{}",ser,self.timestamp);
        Ok(signature::verify(&signature::ED25519,Input::from(&self.public_key),Input::from(msg.as_bytes()),Input::from(self.signature.as_ref()))?)

    }
    pub fn get_action(&self) -> &Action {
        &self.action
    }
    pub fn get_postid(&self) -> Result<ring::digest::Digest> {
        let post = format!("{}{}",serde_json::to_string(&self.action)?,self.timestamp);
        Ok(digest::digest(&digest::SHA512,post.as_bytes()))

    }
    pub fn to_json(&self) -> Result<String> {
        Ok(serde_json::to_string(&self)?)
    }
    pub fn from_json(msg: &str) -> Result<Message> {
        Ok(serde_json::from_str(msg)?)
    }
}

#[derive(Clone,Serialize,Deserialize)]
pub struct Post {
    parent: PostId,
    edited_id: Option<PostId>,
    last_edit: Option<PostContent>,
    body: PostContent,
    children: Vec<Post>,
    timestamp: String,
    pubkey: Key,
    names: Names,
    votes: Votes,
}
#[derive(Clone,Serialize,Deserialize)]
pub struct Votes {
    votes: HashMap<Channel,u32>
}
#[derive(Clone,Serialize,Deserialize)]
pub struct Names {
    self_names: HashMap<Channel,Vec<u8>>,
    trusted_names: HashMap<Channel,(TrustLevel,Vec<u8>)>
}

