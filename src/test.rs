#[test]
fn sig_test() {
    use ring::{signature,rand};
    use untrusted;
    use super::{Message,Action};
    let rng = rand::SystemRandom::new();
    let pkcs8_bytes = signature::Ed25519KeyPair::generate_pkcs8(&rng).unwrap();

    let keypair = signature::Ed25519KeyPair::from_pkcs8(untrusted::Input::from(&pkcs8_bytes)).unwrap();

    let action = Action::Vote(vec![5,4,3,2,1],1);
    let msg = Message::new(&keypair,action.clone()).unwrap();
    msg.verify().unwrap();
    assert_eq!(action,*msg.get_action());
}
#[test]
fn postid_test() {
    use ring::{signature,rand};
    use untrusted;
    use super::{Message,Action};
    let rng = rand::SystemRandom::new();
    let pkcs8_bytes = signature::Ed25519KeyPair::generate_pkcs8(&rng).unwrap();

    let keypair = signature::Ed25519KeyPair::from_pkcs8(untrusted::Input::from(&pkcs8_bytes)).unwrap();


    let action = Action::Vote(vec![5,4,3,2,1],1);
    let msg = Message::new(&keypair,action.clone()).unwrap();
    let postid = msg.get_postid().unwrap();
    assert!(!postid.as_ref().is_empty())
}
