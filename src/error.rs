macro_rules! from {
    ($t:ty,$e:expr) => {
        impl From<$t> for Error {
            fn from(e: $t) -> Error {
                $e(e)
            }
        }
    
    }

}

#[derive(Debug)]
pub enum Error {
    TimeError(::std::time::SystemTimeError),
    SerializeError(::serde_json::Error),
    RingError(::ring::error::Unspecified)
}
from!{
    ::std::time::SystemTimeError,
    |e| Error::TimeError(e)
}
from!{
    ::serde_json::Error,
    |e| Error::SerializeError(e)
}

from!{
    ::ring::error::Unspecified,
    |e| Error::RingError(e)
}

pub type Result<T> = ::std::result::Result<T,Error>;
